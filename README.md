datalife_intrastat
==================

The intrastat module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-intrastat/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-intrastat)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
