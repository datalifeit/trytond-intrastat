# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields, ModelSQL, ModelView, Workflow, Index
from .intrastat import STATES
from trytond.pyson import If, Eval, Bool, PYSONEncoder
from trytond.pool import Pool
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from sql import Null
from sql.aggregate import Sum, Max


DECLARATION_STATES = [
        ('cancelled', 'Cancelled'),
        ('draft', 'Draft'),
        ('scheduled', 'Scheduled'),
        ('processed', 'Processed'),
        ('done', 'Done'),
]


class IntrastatDeclaration(Workflow, ModelSQL, ModelView):
    '''Intrastat Declaration'''
    __name__ = 'intrastat.declaration'
    _rec_name = 'number'

    number = fields.Char('Number', readonly=True)
    from_date = fields.Date('From Date', required=True,
        domain=[If((Eval('from_date')) & (Eval('to_date')),
            ('from_date', '<=', Eval('to_date')),
            (),)],
        states=STATES)
    to_date = fields.Date('To Date', required=True,
        domain=[If((Eval('from_date')) & (Eval('to_date')),
                ('to_date', '>=', Eval('from_date')),
                (),)],
        states=STATES)
    lines = fields.One2Many('intrastat.declaration.line', 'declaration',
        'Lines',
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True)
    description = fields.Char('Description',
        states=STATES)
    summaries = fields.One2Many('intrastat.declaration.summary', 'declaration',
        'Summaries', readonly=True)
    currency_digits = fields.Function(
        fields.Integer('Currency Digits'), 'get_currency_digits')
    state = fields.Selection(DECLARATION_STATES, 'State', required=True,
        readonly=True)
    amount = fields.Function(
        fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2))),
        'get_amount')
    scheduler_available = fields.Function(
        fields.Boolean('Scheduler available'), 'get_scheduler_available')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()
        cls._transitions |= {
            ('draft', 'done'),
            ('draft', 'cancelled'),
            ('draft', 'scheduled'),
            ('scheduled', 'draft'),
            ('scheduled', 'processed'),
            ('processed', 'done'),
            ('done', 'draft'),
            ('cancelled', 'draft'),
            }
        cls._buttons.update({
            'create_lines': {
                'invisible': (Eval('state') != 'draft') | Bool(Eval('lines')),
                'depends': ['state', 'lines'],
            },
            'delete_lines': {
                'invisible': (Eval('state') != 'draft') | ~Bool(Eval('lines')),
                'depends': ['state', 'lines'],
            },
            'draft': {
                'invisible': ~Eval('state').in_(['cancelled',
                    'scheduled', 'done']),
                'depends': ['state'],
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
            },
            'schedule': {
                'invisible': (Bool(Eval('state') != 'draft')
                    | ~Bool(Eval('scheduler_available'))),
                'depends': ['state', 'scheduler_available'],
            },
            'do': {
                'invisible': ~Bool(Eval('state').in_(['draft', 'processed'])),
                'depends': ['state'],
            }
        })

        cls._sql_indexes.add(
            Index(table, (table.number, Index.Equality())))

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company', None)

    def get_scheduler_available(self, name):
        pool = Pool()
        Configuration = pool.get('intrastat.configuration')

        return Configuration(1).scheduler_available

    @classmethod
    @ModelView.button
    def delete_lines(cls, declarations):
        pool = Pool()
        Line = pool.get('intrastat.declaration.line')
        lines = [line for declaration in declarations
            for line in declaration.lines]
        Line.delete(lines)

    @classmethod
    @ModelView.button
    def create_lines(cls, declarations):
        pool = Pool()
        Conf = pool.get('intrastat.configuration')
        Line = pool.get('intrastat.declaration.line')
        InvoiceLine = pool.get('account.invoice.line')

        conf = Conf(1)
        for declaration in declarations:
            lines = []
            domain = declaration._get_invoice_line_domain(conf)
            if domain is None:
                continue
            invoice_lines = InvoiceLine.search(domain)
            lines = declaration._get_lines(invoice_lines, conf)
            if lines:
                Line.save(lines)

    def _get_invoice_line_domain(self, conf):
        if not conf.declare_arrival and not conf.declare_dispatch:
            return None

        company_country = self.company.party.address_get().country
        types = []
        if conf.declare_arrival:
            types.append('in')
        if conf.declare_dispatch:
            types.append('out')
        return [
            ('invoice', '!=', None),
            ('invoice.company', '=', self.company),
            ('invoice.state', 'in', ('posted', 'paid')),
            ('invoice.move.date', '>=', self.from_date),
            ('invoice.move.date', '<=', self.to_date),
            ('invoice.party.addresses', 'where', [
                ('country.eu_member', '=', True),
                ('country', '!=', company_country)],
            ),
            ('type', '=', 'line'),
            ('invoice.type', 'in', types)
        ]

    def _get_lines(self, invoice_lines, conf):
        pool = Pool()
        Line = pool.get('intrastat.declaration.line')

        lines = []
        for invoice_line in invoice_lines:
            if not invoice_line.is_instastat_valid():
                continue

            if invoice_line.product and invoice_line.product.type == 'goods':
                from_address, to_address = invoice_line.intrastat_addresses
                line = Line(
                    type=invoice_line.intrastat_type,
                    date=invoice_line.invoice.move.date,
                    product=invoice_line.product,
                    intrastat_code=(invoice_line.product.intrastat_code
                        or conf.intrastat_code),
                    transport_mode=conf.transport_mode,
                    transaction_=conf.transaction_type,
                    amount=invoice_line.intrastat_amount,
                    incoterm=(invoice_line.intrastat_incoterm
                        or conf.incoterm_rule),
                    declaration=self,
                    invoice_line=invoice_line,
                    origin=invoice_line.intrastat_origin,
                    quantity=invoice_line.intrastat_quantity,
                    net_weight=invoice_line.intrastat_weight,
                    origin_subdivision=(from_address.subdivision
                        if from_address else None),
                    origin_country=(from_address.country
                        if from_address else None),
                    destination_country=(to_address.country
                        if to_address else None),
                    destination_subdivision=(to_address.subdivision
                        if to_address else None)
                )
                lines.append(line)
            else:
                # services and accounts should be distributed among products
                # of same invoice

                goods_lines = invoice_line.get_intrastat_goods_lines()
                goods_amount = sum(l.amount for l in goods_lines)
                if not goods_amount:
                    continue
                for goods_line in goods_lines:
                    from_address, to_address = goods_line.intrastat_addresses
                    amount = invoice_line.intrastat_amount * (
                        goods_line.amount / goods_amount)
                    line = Line(
                        type=goods_line.intrastat_type,
                        date=goods_line.invoice.move.date,
                        product=goods_line.product,
                        intrastat_code=(goods_line.product.intrastat_code
                            or conf.intrastat_code),
                        transport_mode=conf.transport_mode,
                        transaction_=conf.transaction_type,
                        amount=self.company.currency.round(amount),
                        incoterm=(goods_line.intrastat_incoterm
                            or conf.incoterm_rule),
                        declaration=self,
                        invoice_line=invoice_line,
                        origin=goods_line.intrastat_origin,
                        quantity=0,
                        net_weight=None,
                        origin_subdivision=(from_address.subdivision
                            if from_address else None),
                        origin_country=(from_address.country
                            if from_address else None),
                        destination_country=(to_address.country
                            if to_address else None),
                        destination_subdivision=(to_address.subdivision
                            if to_address else None)
                    )
                    lines.append(line)
        return lines

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, declarations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('scheduled')
    def schedule(cls, declarations):
        pass

    @classmethod
    @Workflow.transition('processed')
    def process(cls, declarations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, declarations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, declarations):
        pass

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Config = pool.get('intrastat.configuration')

        vlist = [x.copy() for x in vlist]
        config = Config(1)
        default_company = cls.default_company()
        for values in vlist:
            if values.get('number') is None:
                values['number'] = config.get_multivalue(
                    'intrastat_sequence',
                    company=values.get('company', default_company)).get()
        return super(IntrastatDeclaration, cls).create(vlist)

    def get_currency_digits(self, name):
        if self.company:
            return self.company.currency.digits

    @classmethod
    def get_amount(cls, records, name=None):
        pool = Pool()
        Line = pool.get('intrastat.declaration.line')
        line = Line.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: Decimal('0.0') for r in records}
        cursor.execute(*line.select(
            line.declaration,
            Sum(line.amount),
            where=reduce_ids(line.declaration, list(map(int, records))),
            group_by=line.declaration)
        )

        res.update(dict((x, y) for x, y in cursor.fetchall()))
        return res

    @classmethod
    def process_scheduled_cron(cls):
        declarations = cls.search([('state', '=', 'scheduled')])
        if declarations:
            cls.delete_lines(declarations)
            cls.create_lines(declarations)
            cls.process(declarations)


def _get_order_invoice_line_field(name):
    def order_field(tables):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        field = InvoiceLine._fields[name]
        table, _ = tables[None]
        iline_tables = tables.get('invoice_line')
        if iline_tables is None:
            invoice_line = InvoiceLine.__table__()
            iline_tables = {
                None: (invoice_line,
                    invoice_line.id == table.invoice_line),
                }
            tables['invoice_line'] = iline_tables
        return field.convert_order(name, iline_tables, InvoiceLine)
    return staticmethod(order_field)


class IntrastatDeclarationLine(ModelSQL, ModelView):
    '''Intrastat Declaration Line'''
    __name__ = 'intrastat.declaration.line'

    declaration = fields.Many2One('intrastat.declaration', 'Declaration',
        required=True, ondelete='CASCADE')
    type = fields.Selection([
        ('arrival', 'Arrival'),
        ('dispatch', 'Dispatch'),
        ], 'Type', required=True,
        states=STATES)
    date = fields.Date('Date', states=STATES)
    product = fields.Many2One('product.product', 'Product',
        ondelete='RESTRICT',
        states=STATES)
    intrastat_code = fields.Many2One('intrastat.code', 'Intrastat code',
        domain=[('cn_subheading', '=', True)],
        ondelete='RESTRICT',
        states=STATES)
    origin_country = fields.Many2One('country.country', 'Origin Country',
        ondelete='RESTRICT',
        states=STATES)
    origin_subdivision = fields.Many2One('country.subdivision',
        'Origin subdivision',
        states=STATES)
    destination_country = fields.Many2One('country.country',
        'Destination Country', ondelete='RESTRICT',
        states=STATES)
    destination_subdivision = fields.Many2One('country.subdivision',
        'Destination subdivision',
        states=STATES)
    transaction_ = fields.Many2One('intrastat.transaction.type',
        'Transaction', ondelete='RESTRICT')
    transport_mode = fields.Many2One('intrastat.transport_mode',
        'Transport Mode', ondelete='RESTRICT',
        states=STATES)
    quantity = fields.Float('Quantity',
        digits=(16, Eval('unit_digits', 2)),
        states=STATES)
    unit_digits = fields.Function(fields.Integer('Unit Digits',),
        'on_change_with_unit_digits',)
    net_weight = fields.Float('Net weight',
        digits=(16, Eval('weight_unit_digits', 2)),
        states=STATES)
    weight_unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'get_weight_unit_digits')
    amount = fields.Numeric('Amount',
        digits=(16, Eval('_parent_declaration', {}).get('currency_digits', 2)),
        states=STATES)
    incoterm = fields.Many2One('incoterm.rule', 'Incoterm',
        ondelete='RESTRICT',
        states=STATES)
    shipment = fields.Reference('Shipment', selection='get_shipment',
        states=STATES)
    state = fields.Function(fields.Selection(DECLARATION_STATES,
        'Declaration state'), 'on_change_with_state')
    invoice = fields.Function(
        fields.Many2One('account.invoice', 'Invoice'),
        'get_invoice', searcher='search_invoice_line_field')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice line',
        ondelete='RESTRICT')
    origin = fields.Reference('Origin', selection='get_origin')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()

        cls._sql_indexes.update({
            Index(table, (table.shipment, Index.Equality())),
            Index(table, (table.invoice_line, Index.Equality()))
            })

    @staticmethod
    def default_type():
        return 'arrival'

    @fields.depends('declaration', '_parent_declaration.state')
    def on_change_with_state(self, name=None):
        if self.declaration:
            return self.declaration.state
        return 'draft'

    @fields.depends('product')
    def on_change_with_unit_digits(self, name=None):
        if self.product:
            return self.product.default_uom.digits

    def get_weight_unit_digits(self, name):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Uom = pool.get('product.uom')

        kilogram = Uom(Modeldata.get_id('product', 'uom_kilogram'))
        return kilogram.digits

    @staticmethod
    def _get_shipment():
        return [
            'stock.shipment.in',
            'stock.shipment.out',
            'stock.shipment.out.return',
            'stock.shipment.in.return',
            ]

    @classmethod
    def get_shipment(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_shipment()
        models = IrModel.search([
                ('model', 'in', models),
                ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @classmethod
    def get_invoice(cls, records, name=None):
        return {r.id: r.invoice_line
            and r.invoice_line.invoice.id for r in records}

    @classmethod
    def search_invoice_line_field(cls, name, clause):
        value = clause[0]
        if '.' not in value:
            value += '.rec_name'
        return [('invoice_line.%s' % value, ) + tuple(clause[1:])]

    order_invoice = _get_order_invoice_line_field('invoice')

    @classmethod
    def get_origin(cls):
        InvoiceLine = Pool().get('account.invoice.line')
        return InvoiceLine.get_origin()


class IntrastatDeclarationSummary(ModelSQL, ModelView):
    '''Intrastat Declaration Summary'''
    __name__ = 'intrastat.declaration.summary'

    declaration = fields.Many2One('intrastat.declaration', 'Declaration',
        required=True, readonly=True)
    type = fields.Selection([
        ('arrival', 'Arrival'),
        ('dispatch', 'Dispatch'),
        ], 'Type', readonly=True)
    intrastat_code = fields.Many2One('intrastat.code', 'Intrastat code',
        readonly=True)
    origin_country = fields.Many2One('country.country', 'Origin Country',
        readonly=True)
    destination_country = fields.Many2One('country.country',
        'Destination Country', readonly=True)
    destination_subdivision = fields.Many2One('country.subdivision',
        'Subdivision', readonly=True)
    transaction_ = fields.Many2One('intrastat.transaction.type',
        'Transaction', readonly=True)
    transport_mode = fields.Many2One('intrastat.transport_mode',
        'Transport Mode', readonly=True)
    incoterm = fields.Many2One('incoterm.rule', 'Incoterm', readonly=True)
    quantity = fields.Float('Quantity',
        digits=(16, 2), readonly=True)
    weight = fields.Float('Net weight', readonly=True,
        digits=(16, Eval('weight_unit_digits', 2)))
    amount = fields.Numeric('Amount', readonly=True,
        digits=(16, Eval('currency_digits', 2)))
    currency_digits = fields.Function(
        fields.Integer('Currency Digits'), 'get_currency_digits')
    weight_unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'get_weight_unit_digits')

    @classmethod
    def table_query(cls):
        pool = Pool()
        Line = pool.get('intrastat.declaration.line')
        Declaration = pool.get('intrastat.declaration')

        line = Line.__table__()
        declaration = Declaration.__table__()
        columns = cls._get_columns(line)
        return declaration.join(
            line, condition=declaration.id == line.declaration
            ).select(
                Max(line.id).as_('id'),
                Max(line.create_uid).as_('create_uid'),
                Max(line.create_date).as_('create_date'),
                Max(line.write_uid).as_('write_uid'),
                Max(line.write_date).as_('write_date'),
                *columns,
                group_by=cls._get_group_columns(line)
            )

    @classmethod
    def _get_columns(cls, line):
        return [
            line.declaration,
            line.type,
            line.intrastat_code,
            line.origin_country,
            line.destination_country,
            line.destination_subdivision,
            line.transaction_,
            line.transport_mode,
            line.incoterm,
            Sum(line.quantity).as_('quantity'),
            Sum(line.net_weight).as_('weight'),
            Sum(line.amount).as_('amount'),
        ]

    @classmethod
    def _get_group_columns(cls, line):
        return [
            line.declaration,
            line.type,
            line.intrastat_code,
            line.origin_country,
            line.destination_country,
            line.destination_subdivision,
            line.transaction_,
            line.transport_mode,
            line.incoterm,
        ]

    def get_currency_digits(self, name):
        if self.declaration:
            return self.declaration.currency_digits

    def get_weight_unit_digits(self, name):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Uom = pool.get('product.uom')

        kilogram = Uom(Modeldata.get_id('product', 'uom_kilogram'))
        return kilogram.digits


class InvoiceRelate(Wizard):
    '''Invoice Relate Intrastat declaration line'''
    __name__ = 'intrastat.declaration.invoice_relate'

    start = StateAction('intrastat.act_invoice_line_form')

    def do_start(self, action):
        pool = Pool()
        DeclarationLine = pool.get('intrastat.declaration.line')
        dline = DeclarationLine.__table__()
        cursor = Transaction().connection.cursor()

        line_ids = []
        model_name = Transaction().context['active_model']
        column = dline.id
        if model_name == 'intrastat.declaration':
            column = dline.declaration
        cursor.execute(*dline.select(
            dline.invoice_line,
            where=(
                reduce_ids(column, Transaction().context['active_ids'])
                & (dline.invoice_line != Null)),
            group_by=dline.invoice_line))
        line_ids = [r[0] for r in cursor.fetchall()]
        data = {
            'res_id': line_ids
        }
        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', data['res_id'])])
        return action, data
