# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool


class Line(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_origin(self):
        if self.origin:
            if isinstance(self.origin, self.__class__):
                return self.origin.intrastat_origin
            return self.origin

    @property
    def intrastat_type(self):
        if self.invoice and self.invoice.type:
            type_ = self.invoice.type
        else:
            type_ = self.invoice_type
        return 'dispatch' if type_ == 'out' else 'arrival'

    @property
    def intrastat_addresses(self):
        if self.intrastat_type == 'arrival':
            from_address = self.invoice.invoice_address
            to_address = self.invoice.company.party.address_get('invoice')
        else:
            from_address = self.invoice.company.party.address_get('invoice')
            to_address = self.invoice.invoice_address
        return from_address, to_address

    @property
    def intrastat_incoterm(self):
        if self.invoice.incoterms:
            return self.invoice.incoterms[0].rule
        if self.origin and isinstance(self.origin, self.__class__):
            return self.origin.intrastat_incoterm

    @property
    def intrastat_quantity(self):
        qty = abs(self.quantity)
        if self.amount < 0:
            qty *= -1
        return self.unit.compute_qty(self.unit, qty, self.product.default_uom)

    @property
    def intrastat_amount(self):
        return self.amount

    def is_instastat_valid(self):
        if self.origin and isinstance(self.origin, self.__class__):
            return self.origin.is_instastat_valid()
        company_country = self.invoice.company.party.address_get(
            'invoice').country
        if company_country == self.invoice.invoice_address.country:
            return False
        addresses = self.intrastat_addresses
        eu_address = addresses[0 if self.intrastat_type == 'arrival' else 1]
        if not eu_address.country or not eu_address.country.eu_member:
            return False
        return (
            any(a.country == company_country for a in addresses)
            and not all(a.country == company_country for a in addresses)
        )

    @property
    def intrastat_weight(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Modeldata = pool.get('ir.model.data')
        weight_cat = Modeldata.get_id('product', 'uom_cat_weight')
        kg_uom = Uom(Modeldata.get_id('product', 'uom_kilogram'))

        if self.unit.category.id == weight_cat:
            return Uom.compute_qty(self.unit, self.quantity, kg_uom)

    def get_intrastat_goods_lines(self):
        return [l for l in self.invoice.lines
            if l.product and l.product.type == 'goods'
            and l.is_instastat_valid()]


class Line4(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_weight(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Modeldata = pool.get('ir.model.data')

        weight = super().intrastat_weight
        if (weight is None and self.product.weight):
            kg_uom = Uom(Modeldata.get_id('product', 'uom_kilogram'))
            weight = Uom.compute_qty(self.product.weight_uom,
                self.product.weight * self.intrastat_quantity, kg_uom)
        return weight


class Line5(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @property
    def intrastat_weight(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Modeldata = pool.get('ir.model.data')
        weight_cat = Modeldata.get_id('product', 'uom_cat_weight')
        kg_uom = Uom(Modeldata.get_id('product', 'uom_kilogram'))

        if (self.secondary_unit
                and self.secondary_unit.category.id == weight_cat
                and self.secondary_quantity is not None):
            return Uom.compute_qty(self.secondary_unit,
                self.secondary_quantity, kg_uom)
        return super().intrastat_weight
