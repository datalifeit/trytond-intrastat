# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import intrastat
from . import product
from . import configuration
from . import declaration
from . import invoice
from . import ir


def register():
    Pool.register(
        intrastat.IntrastatCode,
        intrastat.LoadCodesStart,
        intrastat.LoadCodesFinish,
        intrastat.IntrastatTransportMode,
        intrastat.IntrastatTransactionType,
        declaration.IntrastatDeclaration,
        declaration.IntrastatDeclarationLine,
        declaration.IntrastatDeclarationSummary,
        product.Template,
        product.Product,
        configuration.Configuration,
        configuration.ConfigurationSequence,
        configuration.ConfigurationDeclarationValues,
        invoice.Line,
        ir.Cron,
        module='intrastat', type_='model')
    Pool.register(
        intrastat.LoadCodes,
        declaration.InvoiceRelate,
        module='intrastat', type_='wizard')
    Pool.register(
        invoice.Line4,
        module='intrastat', type_='model',
        depends=['product_measurements'])
    Pool.register(
        invoice.Line5,
        module='intrastat', type_='model',
        depends=['account_invoice_secondary_unit'])
